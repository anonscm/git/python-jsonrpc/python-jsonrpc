# -*- coding: utf-8 -*-
import sys, os, io
from jsonrpc import ServiceHandler

class CGIServiceHandler(ServiceHandler):
    def __init__(self, service):
        if service == None:
            import __main__ as service

        ServiceHandler.__init__(self, service)

    def findServiceEndpoint(self, service, method):
      # Handle the methode and bypass the service name
      pos = method.find(".")
      if pos > -1:
        if service == "":
          service = method[:pos]
        method = method[pos + 1:]
      # sys.stderr.write("### {0} - {1}\n".format(service, method))
      return ServiceHandler.findServiceEndpoint(self, method)

    def handleRequest(self, fin=None, fout=None, env=None):
        if fin==None:
            # sys.stdin.reconfigure(encoding='utf-8')
            #fin = sys.stdin
            fin = io.TextIOWrapper(sys.stdin.buffer, encoding='utf-8')
        if fout==None:
            #fout = sys.stdout
            fout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')
        if env == None:
            env = os.environ

        try:
            contLen=int(env['CONTENT_LENGTH'])
            data = fin.read(contLen)
        except Exception as e:
            data = ""

        resultData = ServiceHandler.handleRequest(self, data)

        response = "Content-Type: text/plain; charset=UTF-8\n"
        response += "Content-Length: %d\n\n" % len(resultData)
        response += resultData

        #on windows all \n are converted to \r\n if stdout is a terminal and  is not set to binary mode :(
        #this will then cause an incorrect Content-length.
        #I have only experienced this problem with apache on Win so far.
        if sys.platform == "win32":
            try:
                import  msvcrt
                msvcrt.setmode(fout.fileno(), os.O_BINARY)
            except:
                pass
        #put out the response
        fout.write(response)
        fout.flush()

def handleCGI(service=None, fin=None, fout=None, env=None):
    CGIServiceHandler(service).handleRequest(fin, fout, env)
