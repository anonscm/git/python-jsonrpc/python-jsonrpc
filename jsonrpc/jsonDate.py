#!/usr/bin/env python
"""
qooxdoo JSON Date support

Functions for serialization of datetime objects to JSON Dates in the format
'new Date(Date.UTC(year,month,day[,hour[,minute[,seconds[,milliseconds]]]]))'
and vice versa.

Full timezone support.

Version:     1.1
Author:      Danny W. Adair <[EMAIL PROTECTED]>
Last Change: 20 June 2006
"""
import datetime, time
import json
import re

# Timezone helpers
# See http://docs.python.org/lib/datetime-tzinfo.html

# XXX - Note that this is only done once. A long running server process should
# probably re-create the local timezone every time it's needed, in case daylight
# saving time kicks in while it's running.
STDOFFSET = datetime.timedelta(seconds=-time.timezone)
DSTOFFSET = time.daylight and datetime.timedelta(seconds=-time.altzone) or STDOFFSET
DSTDIFF = DSTOFFSET - STDOFFSET

class LocalTimezone(datetime.tzinfo):
    """A class capturing the platform's idea of local time."""
    def utcoffset(self, dt):
        return self._isdst(dt) and DSTOFFSET or STDOFFSET
    def dst(self, dt):
        return self._isdst(dt) and DSTDIFF or datetime.timedelta(0)
    def tzname(self, dt):
        return time.tzname[self._isdst(dt)]
    def _isdst(self, dt):
        tt = (dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second, dt.weekday(), 0, -1)
        stamp = time.mktime(tt)
        tt = time.localtime(stamp)
        return tt.tm_isdst > 0
local_timezone = LocalTimezone()

class UTC(datetime.tzinfo):
    """UTC time zone."""
    def utcoffset(self, dt):
        return datetime.timedelta(0)
    def tzname(self, dt):
        return 'UTC'
    def dst(self, dt):
        return datetime.timedelta(0)
utc = UTC()

# JSON Date Serialization/Deserialization

class JSONDateError(Exception):
    """Exception for JSON parsing errors."""
    pass

class JSONNotDateError(Exception):
    """Exception for JSON parsing not a date string."""
    pass

def datetime2JSON(dt):
    """
    Converts the given datetime object to a string suitable for JavaScript
    evaluation:
    'new Date(Date.UTC(year,month,day[,hour[,minute[,seconds[,milliseconds]]]]))'

    If the datetime object is timezone-aware, it will be converted to UTC.
    If it is naive (= no timezone information), UTC will be assumed and applied.

    XXX This could be extended to also accept date instead of datetime objects,
    and return correspondingly shorter JavaScript commands for them.

    XXX 'dt.microseconds / 1000' truncates.
    Would rounding - 'int(round(dt.microsecond / 1000.0)) - be better?
    """
    # Apply UTC if datetime is naive, otherwise convert to UTC
    dt = (dt.tzinfo is None) and dt.replace(tzinfo = utc) or dt.astimezone(utc)
    return 'new Date(Date.UTC(%i,%i,%i,%i,%i,%i,%i))' % (dt.year, dt.month - 1, dt.day, dt.hour, dt.minute, dt.second, dt.microsecond / 1000)

def date2JSON(d):
    return 'new Date(Date.UTC(%i,%i,%i,%i,%i,%i,%i))' % (d.year, d.month - 1, d.day, 0, 0, 0, 0)

def parseJSONdatetime(json_dt):
    """
    Parses json datetime string and returns a corresponding array containing values
    like [year, month, day, hour, minute, seconds, ,milliseconds]
    json_dt is expected to be in the format
    'new Date(Date.UTC(year,month,day[,hour[,minute[,seconds[,milliseconds]]]]))'
    or
    'null'

    A JSONDateError exception is raised if it isn't.
    Empty strings also raise this exception; use JavaScript's 'null' to indicate
    no date. 'null' will return None.

    # XXX The json datetime string currently must start with
    'new Date(Date.UTC(' and must end in '))'. This could be less strict to
    allow white space as in JavaScript - regular expressions ftw?

    XXX This could allow for 'date_only' and in such a case return a date object
    instead of a datetime object.

    XXX This could allow for timezone-related parameters, so that the datetime
    could be returned in a different timezone, or as a naive datetime.
    """

    parts = None
    if not json_dt:
        raise JSONDateError('Empty date - use "null" to indicate no date')
    elif json_dt=='null':
        return parts
    try:
        assert json_dt.startswith('new Date(Date.UTC(') and json_dt.endswith('))')
    except AssertionError:
        raise JSONNotDateError('Invalid date format 1')
    try:
        parts = [int(part.strip()) for part in json_dt.split('(')[2].split(')')[0].split(',')]
        num_parts = len(parts)
        if num_parts > 7:
            raise ValueError
    except (IndexError, ValueError):
        raise JSONDateError('Invalid date format 2')
    if num_parts < 3:
        raise JSONDateError('Not enough arguments')
    elif num_parts > 7:
        raise JSONDateError('Too many arguments')
    elif num_parts == 7:
        # datetime constructor will take microseconds
        parts[6] = parts[6] * 1000
    # correct month value according to JS Date object
    parts[1] = parts[1] + 1
    return parts

def JSON2datetime(parts):
    """
    Converts the array from parseJSONdatetime to a corresponding datetime
    """
    #print '###', parts
    if parts == None:
      return parts
    # An invalid date might have been specified (29 Feb in non-leap year etc.)
    try:
        # Construct as timezone-aware UTC
        return datetime.datetime(tzinfo=utc, *parts)
    except ValueError as msg:
        raise JSONDateError('Invalid date: {0}'.format(msg))


##########################################################################
#        Encoder and Decoder classes for date in Javascript
##########################################################################
class __JSONDateEncoder__(json.JSONEncoder):
  """
  Specific encoder to transform datetime and date python objects into
  Javascript Date objects
  """
  def default(self, obj):
    if isinstance(obj, datetime.datetime):
      out = datetime2JSON(obj)
      return out
    elif isinstance(obj, datetime.date):
      out = date2JSON(obj)
      return out
    else:
      return super(__JSONDateEncoder__, self).default(obj)


class __JSONDateDecoder__(json.JSONDecoder):
  """
  Specific decoder to transform Javascript Date objects into
  python datetime objects
  """

  def __init__(self, **kw):
    super(__JSONDateDecoder__, self).__init__(object_hook=self.parse_date, **kw)

  def decode(self, obj):
    """
    Pattern to recognize date object in JavaScript: new Date(Date.UTC(yyyy, mm, dd, hh, mm, ss, mmm)).
    Collect year, month and day values in the groupdict of a RegexMatch object.
    """
    inner_mask = r'(\s*\d+\s*,?\s*){3,7}'
    inner_mask = r'('
    inner_mask += r'\s*(?P<year>\d+)\s*,\s*'
    inner_mask += r'\s*(?P<month>\d+)\s*,\s*'
    inner_mask += r'\s*(?P<day>\d+)\s*,?\s*'
    inner_mask += r'(\s*\d+\s*,?\s*){0,4}'
    inner_mask += r')'
    date_mask = re.compile(r'\s*"?\s*new\s+\Date\(\s*Date\.UTC\(\s*(' + inner_mask + r')\s*\)\s*\)\s*"?\s*')

    # Replace value of date by a JSON object
    out = date_mask.sub('{"__type__" : "datetime","year":\g<year>,"month":\g<month>,"day":\g<day>}', obj)
    return super(__JSONDateDecoder__, self).decode(out)

  def parse_date(self, d):
    if '__type__' not in d:
      return d
    type = d.pop('__type__')
    if type == 'datetime':
      d["month"] += 1
      return datetime.datetime(**d)
    else:
      # Oops... better put this back together.
      d['__type__'] = type
      return d
##########################################################################

##########################################################################
#       Override dump/dumps and load/loads functions
##########################################################################
# From Python objects to JSON
def dump(obj, **kw):
  #out = json.dump(obj, cls=__JSONDateEncoder__, ensure_ascii=False, **kw)
  out = json.dump(obj, cls=__JSONDateEncoder__, **kw)
  return out.strip('"')

def dumps(obj, **kw):
  #out = json.dumps(obj, cls=__JSONDateEncoder__, ensure_ascii=False, **kw)
  out = json.dumps(obj, cls=__JSONDateEncoder__, **kw)
  inner_mask = r'(\s*\d+\s*,?\s*){3,7}'
  date_mask = re.compile(r'(?P<open>")(?P<content>new\s+\Date\(\s*Date\.UTC\(\s*' + inner_mask + r'\)\))(?P<close>")')
  out = date_mask.sub('\g<content>', out)
  return out


# From JSON to Python objects
def load(obj, **kw):
  out = json.load(obj, cls=__JSONDateDecoder__, **kw)
  return out

def loads(obj, **kw):
  out = json.loads(obj, cls=__JSONDateDecoder__, **kw)
  return out
##########################################################################
