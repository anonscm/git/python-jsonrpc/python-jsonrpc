# -*- coding: utf-8 -*-
"""
  Copyright (c) 2007 Jan-Klaas Kollhof

  This file is part of jsonrpc.

  jsonrpc is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation; either version 2.1 of the License, or
  (at your option) any later version.

  This software is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this software; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

from jsonrpc import loads, dumps, JSONEncodeException
import sys

def ServiceMethod(fn):
    fn.IsServiceMethod = True
    return fn

class ServiceException(Exception):
    pass

class ServiceRequestNotTranslatable(ServiceException):
    pass

class BadServiceRequest(ServiceException):
    pass

class ClassMethodNotFound(ServiceException):
    def __init__(self, name):
        self.methodName = name

    def __str__(self):
      return self.methodName

class ServiceMethodNotFound(ServiceException):
    def __init__(self, name):
        self.methodName=name

    def __str__(self):
        return self.methodName

class ServiceHandler(object):

    def __init__(self, service):
        self.service=service

    def handleRequest(self, json):
        err=None
        result = None
        id_=''
        serviceName=''
        args = None
        server_data = None
        try:
            req = self.translateRequest(json)
        except ServiceRequestNotTranslatable as e:
            err = e
            req={'id':id_}

        if err == None:
            if 'service' in req:
              serviceName = req['service']
            if 'server_data' in req:
              server_data = req['server_data']
            try:
                id_ = req['id']
                methName = req['method']
                args = req['params']
                if ('.' in methName and not serviceName):
                    (serviceName, methName) = methName.split(".")
            except:
                err = BadServiceRequest(json)

        if err == None:
            try:
                meth = self.findServiceEndpoint(serviceName, methName)
            except Exception as e:
                err = e

        if err == None:
            try:
                result = self.invokeServiceEndpoint(meth, args, server_data)
            except Exception as e:
                err = e

        resultdata = self.translateResult(result, err, id_)

        return resultdata

    def translateRequest(self, data):
        try:
            req = loads(data)
        except:
            sys.stderr.write("ServiceRequestNotTranslatable raised\n")
            # if (data.has_key("params")):
            #       data.pop("params")
            raise ServiceRequestNotTranslatable(data)
        return req

    def findServiceEndpoint(self, name):
        try:
            meth = getattr(self.service, name)
            if getattr(meth, "IsServiceMethod"):
                return meth
            else:
                raise ServiceMethodNotFound(name)
        # except AttributeError as e:
        except AttributeError:
            raise ClassMethodNotFound(name)
            #raise ServiceMethodNotFound(e.__class__.__name__ + ': ' + e.__str__())

    def invokeServiceEndpoint(self, meth, args, server_data):
        # TODO Implement the use of server_data (ex: for authentication)
        #if (server_data):
          #sys.stderr.write("Got server data\n")
          #args.insert(0, server_data)
        return meth(*args)

    def translateResult(self, rslt, err, id_):
        err2 = None
        if err != None:
            err2 = {"origin": 1, "code": err.__class__.__name__, "message": err.__str__()}
            rslt = None

        try:
            data = dumps({"result":rslt,"id":id_,"error":err2})
        except JSONEncodeException as e:
            err2 = {"origin": 1, "code": e.__class__.__name__, "message": e.__str__()}
            data = dumps({"result":None, "id":id_,"error":err2})

        return data
