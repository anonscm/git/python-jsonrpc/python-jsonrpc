import sys, os
from werkzeug.wrappers import Request, Response
from jsonrpc import ServiceHandler, ServiceException, ServiceMethod
from json import dumps

class ServiceImplementaionNotFound(ServiceException):
  pass

class WsgiServiceHandler(ServiceHandler):
  def __init__(self, req):
    self.req = req
    ServiceHandler.__init__(self, None)

  def findServiceEndpoint(self, service, method):
    req = self.req

    (modulePath, fileName) = os.path.split(req.path)
    moduleName = os.path.splitext(fileName)[0]

    if modulePath[0] == '/':
      modulePath = modulePath[1:]

    if not os.path.exists(os.path.join(modulePath, moduleName + ".py")):
      raise ServiceImplementaionNotFound()
    else:
      if modulePath and not modulePath in sys.path:
        sys.path.insert(0, modulePath)

      import importlib
      module = importlib.import_module(moduleName)
      if hasattr(module, "service"):
        self.service = module.service
      elif hasattr(module, "Service"):
        self.service = module.Service()
      elif service != '' and hasattr(module, service):
        _service = getattr(module, service)
        self.service = _service()
      else:
        self.service = module
    return ServiceHandler.findServiceEndpoint(self, method)

  def handleRequest(self):
    req = self.req
    data = req.get_data()
    resultData = ServiceHandler.handleRequest(self, data)
    return resultData

def application(environ, start_response):
  request = Request(environ)
  text = WsgiServiceHandler(request).handleRequest()
  response = Response(text, mimetype='json/application')
  return response(environ, start_response)

if __name__ == '__main__':
  from werkzeug.serving import run_simple
  run_simple('localhost', 8082, application)
